provider "aws" {
  version = "~> 3.0"
  profile = var.iam_profile
  region  = var.aws_region
}
/*
resource "aws_s3_bucket" "terraform_remote_state_bucket" {
  bucket = "bmrn-sftp-terraform-remote-state-config"
  # Enable versioning so we can see the full revision history of our state files
  versioning {
    enabled = true
  }
  # Enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
*/
