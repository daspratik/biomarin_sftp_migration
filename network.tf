resource "aws_vpc" "bmrn" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_internet_gateway" "bmrn_igw" {
  vpc_id = aws_vpc.bmrn.id
  tags = {
    Name = "${var.vpc_name}-igw"
  }
  depends_on = ["aws_vpc.bmrn"]
}

resource "aws_subnet" "public" {
  count                   = length(var.public_subnet_cidr)
  vpc_id                  = aws_vpc.bmrn.id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = lookup(var.bmrn_az, count.index)
  map_public_ip_on_launch = true

  tags = {
    count = length(var.public_subnet_cidr)
    Name  = "${var.vpc_name}-public-subnet-${lookup(var.public_name, count.index)}"
  }
  depends_on = ["aws_vpc.bmrn"]
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.bmrn.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.bmrn_igw.id
  }
  tags = {
    Name = "${var.vpc_name} - Public Route Table"
  }
  depends_on = ["aws_vpc.bmrn", "aws_internet_gateway.bmrn_igw"]
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_subnet_cidr)
  route_table_id = aws_route_table.public_rt.id
  subnet_id      = element(aws_subnet.public.*.id, count.index)
}
