resource "aws_s3_bucket" "log_bucket" {
  bucket = "bmrn-non-validated-sftp-log-bucket"
  acl    = "log-delivery-write"
}

resource "aws_s3_bucket" "sftp_s3" {
  bucket = var.sftp_bucket_name
  acl    = "private"
  versioning {
    enabled = true
  }
  tags = {
    Name     = var.sftp_bucket_name
    Owner    = var.owner_name
    DataType = "SFTP files"
  }
  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "non-validated-sftp-logs/"
  }
  # Default Encryption is AES256 instead of SSE-KMS or SSE-C
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket              = aws_s3_bucket.sftp_s3.id
  block_public_acls   = true
  block_public_policy = true
}
