variable "aws_region" {
  type        = string
  description = "This is the AWS Region to create the Terraform resources"
  default     = "us-west-2"
}

variable "iam_profile" {
  type        = string
  description = "This is the IAM user profile to create Terraform Resources"
  default     = "tf_admin"
}

variable "vpc_name" {
  type        = string
  description = "This variable is to assign the name to default VPC"
  default     = "BMRN-SFTP"
}

variable "vpc_cidr" {
  type        = string
  description = "This is the CIDR block for VPC"
  default     = "10.100.200.0/24"
}

variable "public_subnet_cidr" {
  description = "Subnet CIDRs for Public subnets"
  default     = ["10.100.200.0/25", "10.100.200.128/25"]
  type        = list(string)
}

variable "public_name" {
  description = "name of Public subnets"
  type        = map
  default = {
    "0" = "one"
    "1" = "two"
  }
}

variable "bmrn_az" {
  description = "AZs in default Region"
  type        = map
  default = {
    "0" = "us-west-2a"
    "1" = "us-west-2b"
  }
}

variable sftp_bucket_name {
  description = "name of the sftp server s3 bucket"
  type        = "string"
  default     = "bmrn-sftp-s3"
}

variable transfer_server_name {
  description = "name of the non_validated sftp server"
  type        = "string"
  default     = "bmrn_sftp_non_validated_server"
}

variable sftp_user {
  description = "sftp users' list"
  type        = list(string)
  default     = ["sftp-user01", "sftp-user02", "sftp-user03"]
}

variable "transfer_server_ssh_keys" {
  description = "SSH Key(s) for transfer server user(s)"
  type        = "map"
  default = {
"0" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDuhLvNqfC/XxA9Jg1qx0AuMDZkdc+TheBYoSrnCtAKugDXAU7eAK8nbjXnAkEc+un+jYBFzFf6rvQ8bTec/TAl4zKf0l3eK/V1O2LYV+49X1Aeo3eB0M0xBUyo2x2fdwkmfoa6Rdb7kBL1ZTHf84aZTpvZA1jGMpHGf0BCpegPcpX3qehm/Tn6E6kgd23XT7cevHXy0Vs/HlS1nNo3RFvIiSrxyPW1/w38vQpesQx9pShlqS6kjTefNGyrCNPvcrcO/HHp4JbGkFBKTcwIYIKmtVjt/2YaFyQ9PSbU/p/wCbBGZop7HP/yld8B+TVqFD50MSwrZ1zv/qA1PxH3MhraubSNdvkvaLVkj3DxzKGnTo0kMMUDpTXKVFc+HcEtjP1yc9/ezYgCIJ9w3YlzoNk9b0bT9TrHSjjyejwGDrdH2Vo7E1xQMLeP0GzDzoHuBo4pzhPKgL2xzfpuEoUCLU+BBkZuO6FtupUDXEomOHNN1LzQ9tOfjOlNSqIk8bJ1TrXmLydpfRCGPTVNpByudVGCZGKcMXFsuhRXpZ5TIMvH6JMBRzKUSo0LNnJjngaJj6D/5Qils4INaOxQRzNyVNFlCt113Q6x0hYv37O49aDYeQn/5g2NoynXkA49KUT0n64zjKaxNvYAnsomNMZWz+SZEj/fBEJRAtiOYnqqdr4/EQ== prati@DESKTOP-C6MK408"
"1" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDuAj69u78lf9XRgBPfXAmJAcZ8xlLcwWMRdj6vuYya4IQqDviMCAWX+PCi5lHjDoBN8OF+tRbuC4Kr1M1ZApZLSZL6KF7SSqnQbIAzPPl5aGEbWToLDFM1wPwd012qBuNJ1PGIxbx1TF/rQlAOe3UjkmHIiSaP76zBW8ath/0n/2apxysbhTEmuegS4ETOPU1Uq4tUCBlE+arZBsXiwUD/P8WOo1UjL6rjnAoh6kxyuov9OB3zAKiKzQIucQNbQiWl/WSZk5zLmg60/y9i9ZOJTnRdAXvXR16wsdA3NiAXSE/Xa35L/LwtCFvQAgq4Hid0+4MAbLWpNB8KxXpUEjnaVx5EJsmtu8caqIb/mlxRJCRy6qMnhnFSRCyisWe6RDbJkBIF/Rk7O/LF39adHkHFWvWu1za7k084ibbQlhj/nJIa/BO65d1byycbmziTNShOsr9nB55Qwf3cvyVmJdJBPELhIYjTay/x18SwCKEhX6C9rFEJr6wim00aADWE9YaEN8lM1ro+Ff5lM8fggtZrYEsy1oJLm/JWvEFxCFJOK+TmoLVjSWaGtPY8OD1JlQYgEpmM1hKYMqNxdcCRTDSbTIhUf0z2eKRNsRrxeiiHROI84nvExn4EFNwmToJy3KU47kAAgKTpMMVIw65aDYUkONgG8EWa7jiSA0YNYdTu6Q== prati@DESKTOP-C6MK408"
"2" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCvxuvhx06EWe4iSBBHmpEiv34U38DoE5KMJ2j+kBK7irNd8UCbZfR3fjf3amaoGRUVSykZovWFV7hYswF/TcRqtK9ksxic1IVVAsfdnq4LuKvywDqMeXd5YA06fjJ4jiuByvP07vypMsAB3tlQGeutyELjXNzUKK65/Nk2EawOcf3TDxLO0T5KaBwBfMYulLNmzYfD1yJrAofu0VKBRE0znoE09CLLxY1hctZVi7RToxMliusSAS3YJGYCi1N39fkLreOWlsL42RqwfJpWCUzQ57xA1cWZe6LAwwOdY2B24N9Xy6DQTHmJXMtTVXSA1RnJ2b5ZM1Mh8Ywv0Vv10hqQZbldklV1wNQ+gIhpjrknUleIBcoCqPjTjnENuHaKIvpkz/6NxdtFJYk5ycAPinQ8AxqceAB3kk9NOKtrw+4i28SnPiumjcMXIp35JJhAR7siuT7efzljcBvzDKA1IkgnzTGqCmEMl6fqzsUdZg6qTaLhEhG9FQPjNVAS822heO71hT5iDfS+18H5C8/N529q56KAbpIsT9h8vvxnKExOx7LLKFvaE3FoYVUfxpD3GnavW26dYG4jVl5U0xkcY8K/2yBb5G7iRlfmgPBRcR2M9OUZbAUYrI6YFcEBYJ8JrcupfSeIP6V5hAekBngv5dTkR7AkomWBu7wmVAwC2sBcZw== prati@DESKTOP-C6MK408"
  }
}

variable "owner_name" {
  default = "BIOMARIN PHARMACEUTICALS"
}
