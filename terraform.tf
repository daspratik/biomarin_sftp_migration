terraform {
  backend "s3" {
    bucket  = "terraform-remote-state-config-bucket"
    key     = "terraform.tfstate"
    profile = "tf_admin"
    region  = "us-west-2"

    # dynamodb_table = "bmrn-sftp-terraform-remote-state-config-locks"
    # encrypt        = true
  }
}
