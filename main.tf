resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.bmrn.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "eip" {
  count            = length(var.bmrn_az)
  vpc              = true
  public_ipv4_pool = "amazon"

  tags = {
    Name = "eip-${count.index + 1}"
    Owner = var.owner_name
  }

  depends_on = [
    "aws_internet_gateway.bmrn_igw"
  ]
}
/*
resource "aws_vpc_endpoint" "vpce" {
  vpc_id              = aws_vpc.bmrn.id
  service_name        = "com.amazonaws.${var.aws_region}.transfer.server"
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  subnet_ids          = [aws_subnet.public[0].id, aws_subnet.public[1].id]

  security_group_ids = [
    aws_default_security_group.default.id,
  ]

  tags = {
    Name = "${var.vpc_name}-vpce"
  }
}
*/
# Creating AWS SFTP Server Resource
 resource "aws_transfer_server" "transfer_server" {
  endpoint_type = "VPC"
  identity_provider_type = "SERVICE_MANAGED"
  logging_role           = aws_iam_role.transfer_server_role.arn

  tags = {
    Name = var.transfer_server_name
    Owner = var.owner_name
    Environment = "Non-Validated"
  }

endpoint_details {
  vpc_id                 = aws_vpc.bmrn.id
  subnet_ids = [aws_subnet.public[0].id, aws_subnet.public[1].id]
  address_allocation_ids = [aws_eip.eip[0].id, aws_eip.eip[1].id]
  }
}

# create a folder for each sftp user in S3 bucket which was previously created.
resource "aws_s3_bucket_object" "s3_folder" {
  count        = length(var.sftp_user)
  depends_on   = [aws_s3_bucket.sftp_s3]
  bucket       = var.sftp_bucket_name
  key          = "/${var.sftp_user[count.index]}/"
  content_type = "application/x-directory"
}

# create sftp user and assign home directory for the user
resource "aws_transfer_user" "transfer_server_user" {
  count = length(var.sftp_user)
  server_id      = aws_transfer_server.transfer_server.id
  user_name      = element(var.sftp_user, count.index)
  role           = aws_iam_role.transfer_server_role.arn
  home_directory = "/${var.sftp_bucket_name}/${var.sftp_user[count.index]}"
}

# create ssh keys for the sftp users
resource "aws_transfer_ssh_key" "transfer_server_ssh_key" {
  count = length(var.sftp_user)
  server_id = aws_transfer_server.transfer_server.id
  user_name = element(aws_transfer_user.transfer_server_user.*.user_name, count.index)
  body      = lookup(var.transfer_server_ssh_keys, count.index)
}