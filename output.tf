output "sftp_server_fqdn" {
  value       = join(".", [aws_transfer_server.transfer_server.id, "server.transfer", var.aws_region, "amazonaws.com"])
  description = "The FQDN of the SFTP Server"
}
/*
output "transfer_server_id" {
  value = aws_transfer_server.transfer_server.id
}

output "s3_bucket_arn" {
  value       = aws_s3_bucket.terraform_remote_state_bucket.arn
  description = "The ARN of the S3 bucket"
}
*/
output "sftp_bucket_arn" {
  value       = aws_s3_bucket.sftp_s3.arn
  description = "The ARN of the SFTP bucket"
}

output "public_subnet_ids" {
  value = [aws_subnet.public[0].id, aws_subnet.public[1].id]
}

output "eip_id" {
  value = [aws_eip.eip[0].id, aws_eip.eip[1].id]
}

output "eip_ip_public" {
  value = [aws_eip.eip[0].public_ip, aws_eip.eip[1].public_ip]
}

output "eip_ip_private" {
  value = [aws_eip.eip[0].private_ip, aws_eip.eip[1].private_ip]
}
